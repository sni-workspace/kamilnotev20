import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {persistor, store} from 'src/store';
import tailwind from 'tailwind-rn';

import HomeScreen from 'src/screens/NotesTab/HomeScreen';
import NewNoteScreen from 'src/screens/NotesTab/NoteScreen/NewNoteScreen';
import NoteScreen from 'src/screens/NotesTab/NoteScreen/NoteScreen';

import ToDoScreen from '/src/screens/ToDoTab';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

function NotesScreenStack() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="NoteHome" component={HomeScreen} />
      <HomeStack.Screen name="NewNote" component={NewNoteScreen} />
      <HomeStack.Screen name="Note" component={NoteScreen} />
    </HomeStack.Navigator>
  );
}

function ToDoScreenStack() {
  return (
    <ToDoStack.Navigator>
      <ToDoStack.Screen name="ToDoHome" component={ToDoScreen} />
    </ToDoStack.Navigator>
  );
}

const Tab = createBottomTabNavigator();
const HomeStack = createNativeStackNavigator();
const ToDoStack = createNativeStackNavigator();

class App extends React.PureComponent {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <NavigationContainer>
            <Tab.Navigator
              screenOptions={({route}) => ({
                tabBarStyle: {
                  alignItems: 'center',
                  justifyContent: 'center',
                  paddingVertical: 1,
                },
                tabBarButton: props => {
                  const label = props.to.split('/');
                  let iconType = '';

                  if (label[1] === 'Notatki') {
                    iconType = 'note';
                  } else if (label[1] === 'Tasks') {
                    iconType = 'format-list-checks';
                  }
                  return (
                    <TouchableOpacity
                      onPress={props.onPress}
                      style={[
                        tailwind(
                          'flex flex-col items-center justify-center w-1/5 h-full rounded-full',
                        ),
                        props.accessibilityState.selected === true && {
                          backgroundColor: '#999999',
                        },
                      ]}>
                      <Icon name={iconType} size={20} />
                      <Text>{label[1]}</Text>
                    </TouchableOpacity>
                  );
                },
                tabBarActiveTintColor: 'black',
                tabBarActiveBackgroundColor: 'gray',
                tabBarInactiveTintColor: 'black',
                headerShown: false,
              })}>
              <Tab.Screen
                name="Notatki"
                type="note"
                component={NotesScreenStack}
              />
              <Tab.Screen
                name="Tasks"
                type="clear"
                component={ToDoScreenStack}
              />
            </Tab.Navigator>
          </NavigationContainer>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
