import React from 'react';
import {Alert} from 'react-native';

const removingItem = (app, type, onPress, id) => {
  const areYouSure =
    'Czy na pewno chcesz usunąc ' +
    (type === 'note' ? 'tą notatkę' : 'ten task') +
    '?';
  const canceled =
    'Anulowano usuwanie ' + (type === 'note' ? 'notatki' : 'taska') + '.';
  const removed = 'Usunięto ' + (type === 'note' ? 'notatkę' : 'task') + '!';

  Alert.alert(app, areYouSure, [
    {
      text: 'Anuluj',
      onPress: () => Alert.alert(app, canceled),
      style: 'cancel',
    },
    {
      text: 'Usuń',
      onPress: () => {
        onPress(id);
        Alert.alert(app, removed);
      },
    },
  ]);
};

export default removingItem;
