import React from 'react';
import {
  ScrollView,
  SafeAreaView,
  View,
  TextInput,
  TouchableOpacity,
  Text,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {connect} from 'react-redux';

import {notesActions} from 'src/actions';
import ColorSelect from 'src/components/Notes/ColorSelect';
import Button from 'src/components/Utils/Button';
import Input from 'src/components/Notes/Input';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

class NNScreen extends React.PureComponent {
  state = {
    noteTitle: '',
    noteText: '',
    selectedColor: 'white',
  };

  componentDidMount() {
    this.props.navigation.setOptions({
      headerShown: true,
      headerTransparent: false,
      headerShadowVisible: false,
      title: 'Nowa notatka',
      headerTitleAlign: 'center',
      headerRight: this.savingButton,
      headerLeft: this.backButton,
    });
  }

  saveNote = () => {
    const {noteText, noteTitle, selectedColor} = this.state;
    this.props.addNote(noteText, noteTitle, selectedColor);
    this.props.navigation.goBack();
  };

  backButton = () => {
    return (
      <Button
        type="chevron-left"
        size={30}
        onPress={() => this.props.navigation.goBack()}
      />
    );
  };

  savingButton = () => {
    return (
      <TouchableOpacity onPress={this.saveNote}>
        <Icon name="check" size={30} color="black" />
      </TouchableOpacity>
    );
  };

  changeTitle = string => {
    this.setState({noteTitle: string});
  };

  changeText = string => {
    this.setState({noteText: string});
  };

  changeColor = color => {
    this.props.navigation.setOptions({
      headerStyle: {
        backgroundColor: color,
      },
    });

    this.setState({selectedColor: color});
  };

  render() {
    const {noteText, noteTitle, selectedColor} = this.state;
    return (
      <SafeAreaView
        style={[
          tailwind('flex-1 w-full items-center justify-start px-2'),
          {backgroundColor: this.state.selectedColor},
        ]}>
        <Input
          title={noteTitle}
          text={noteText}
          selectedColor={selectedColor}
          changeColor={this.changeColor}
          changeTitle={this.changeTitle}
          changeText={this.changeText}
          navigation={this.props.navigation}
        />
      </SafeAreaView>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  addNote: (note, title, color) =>
    dispatch(notesActions.addNote(note, title, color)),
});

const NewNoteScreen = connect(null, mapDispatchToProps)(NNScreen);

export default NewNoteScreen;
