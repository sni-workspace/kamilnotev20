import React from 'react';
import {View, Text, SafeAreaView, Alert} from 'react-native';
import tailwind from 'tailwind-rn';
import {connect} from 'react-redux';

import {notesActions} from 'src/actions';

import Input from 'src/components/Notes/Input';
import Button from 'src/components/Utils/Button';

class NScreen extends React.PureComponent {
  state = {
    editing: false,
    selectedColor: this.props.route.params.noteData.color,
    noteTitle: this.props.route.params.noteData.title,
    noteText: this.props.route.params.noteData.text,
  };

  componentDidMount() {
    this.props.navigation.setOptions({
      headerShown: true,
      headerTransparent: false,
      headerShadowVisible: false,
      title: this.state.noteTitle,
      headerTitleAlign: 'center',
      headerStyle: {
        backgroundColor: this.state.selectedColor,
      },
      headerRight: this.renderRightButton,
      headerLeft: this.renderLeftButton,
    });
  }

  componentDidUpdate() {
    const {noteTitle, selectedColor} = this.state;
    this.props.navigation.setOptions({
      title: noteTitle,
      headerStyle: {
        backgroundColor: selectedColor,
      },
    });
  }

  changeTitle = string => {
    this.setState({noteTitle: string});
  };

  changeText = string => {
    this.setState({noteText: string});
  };

  saveNote = () => {
    const {noteText, noteTitle, selectedColor} = this.state;
    this.props.editNote(
      this.props.route.params.noteData.id,
      noteText,
      noteTitle,
      selectedColor,
    );
  };

  removeNote = id => {
    Alert.alert('KamilNote', 'Czy na pewno chcesz usunąć tą notatkę ?', [
      {
        text: 'Anuluj',
        onPress: () => Alert.alert('KamilNote', 'Anulowano usuwanie notatki'),
        style: 'cancel',
      },
      {
        text: 'Usuń',
        onPress: () => {
          this.props.removeNote(id);
          this.props.navigation.navigate('NoteHome');
          Alert.alert('KamilNote', 'Usunięto notatkę');
        },
      },
    ]);
  };

  changeColor = color => {
    this.setState({selectedColor: color});
  };

  renderLeftButton = () => {
    const {editing} = this.state;
    if (editing) {
      return (
        <Button
          type="delete"
          size={30}
          onPress={() => {
            this.removeNote(this.props.route.params.noteData.id);
          }}
        />
      );
    } else {
      return (
        <Button
          type="chevron-left"
          size={40}
          onPress={() => this.props.navigation.goBack()}
        />
      );
    }
  };

  renderRightButton = () => {
    const {editing} = this.state;
    if (editing) {
      return (
        <Button
          type="check"
          size={30}
          onPress={() => {
            this.saveNote();
            this.setState({editing: !editing});
          }}
        />
      );
    } else {
      return (
        <Button
          type="square-edit-outline"
          size={30}
          onPress={() => this.setState({editing: !editing})}
        />
      );
    }
  };

  renderInput = () => {
    const {noteTitle, noteText, selectedColor} = this.state;
    return (
      <Input
        title={noteTitle}
        text={noteText}
        selectedColor={selectedColor}
        changeColor={this.changeColor}
        changeTitle={this.changeTitle}
        changeText={this.changeText}
        navigation={this.props.navigation}
      />
    );
  };

  renderText = () => {
    return (
      <View style={tailwind('flex-1 w-full mt-2')}>
        <Text>{this.state.noteText}</Text>
      </View>
    );
  };

  renderContent = () => {
    return this.state.editing ? this.renderInput() : this.renderText();
  };

  render() {
    return (
      <SafeAreaView
        style={[
          tailwind('flex-1 w-full items-center justify-start px-2'),
          {backgroundColor: this.state.selectedColor},
        ]}>
        {this.renderContent()}
      </SafeAreaView>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  editNote: (id, note, title, color) =>
    dispatch(notesActions.editNote(id, note, title, color)),
  removeNote: id => dispatch(notesActions.removeNote(id)),
});

const NoteScreen = connect(null, mapDispatchToProps)(NScreen);

export default NoteScreen;
