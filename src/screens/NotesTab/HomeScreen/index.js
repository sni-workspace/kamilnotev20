import React from 'react';
import {View, Text, TouchableOpacity, ScrollView, Alert} from 'react-native';
import {connect} from 'react-redux';
import tailwind from 'tailwind-rn';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {notesActions} from 'src/actions';

import NotePreview from './NotePreview';
import Button from 'src/components/Utils/Button';
import removingItem from 'src/functions/utils/removingItem';

class HScreen extends React.PureComponent {
  componentDidMount() {
    this.props.navigation.setOptions({
      headerShown: true,
      headerTransparent: true,
      headerStyle: {
        backgroundColor: 'white',
      },
      headerShadowVisible: false,
      title: 'KamilNote',
      headerTitleAlign: 'center',
    });
  }

  navigateToNote = note => {
    this.props.navigation.navigate('Note', {noteData: note});
  };

  navigateToNewNote = () => {
    this.props.navigation.push('NewNote');
  };

  renderNotes = () => {
    if (this.props.notes.length === 0) {
      return (
        <NotePreview
          mt
          title="Brak notatek, dodaj jakąś :)"
          onPress={this.navigateToNewNote}
          removeItem={() => {
            return false;
          }}
        />
      );
    } else {
      return Object.entries(this.props.notes).map(e => {
        return (
          <NotePreview
            mt={e[0] == 0}
            key={e[1].id}
            id={e[1].id}
            text={e[1].text}
            title={e[1].title}
            color={e[1].color}
            onPress={() => this.navigateToNote(e[1])}
            removeItem={id =>
              removingItem('KamilNote', 'note', this.props.removeNote, id)
            }
          />
        );
      });
    }
  };

  render() {
    return (
      <View
        style={tailwind('flex-1 w-full items-center justify-start bg-white')}>
        <ScrollView
          scrollEnabled
          style={tailwind('w-full')}
          contentContainerStyle={tailwind('items-center px-4')}>
          {this.renderNotes()}
        </ScrollView>
        <Button
          onPress={this.navigateToNewNote}
          style={tailwind(
            'absolute bottom-0 right-0 m-4 p-0 border-black border rounded-full bg-white',
          )}
          type="plus"
          size={50}
          color="black"
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  notes: state.notes,
});

const mapDispatchToProps = dispatch => ({
  removeNote: id => dispatch(notesActions.removeNote(id)),
});

const HomeScreen = connect(mapStateToProps, mapDispatchToProps)(HScreen);

export default HomeScreen;
