import React from 'react';
import {View, Text, TouchableOpacity, Platform} from 'react-native';

import tailwind from 'tailwind-rn';

export default class NotePreview extends React.PureComponent {
  prepareTitle = text => {
    let preparedTitle = '';
    let maxLength = 31;
    if (text.length >= maxLength) {
      preparedTitle = text.slice(0, maxLength);
      preparedTitle = preparedTitle.concat('...');
    } else {
      preparedTitle = text;
    }
    return preparedTitle;
  };

  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        onLongPress={() => this.props.removeItem(this.props.id)}
        style={[
          tailwind('w-full border-black border rounded p-6 my-2'),
          this.props.mt && Platform.OS === 'android'
            ? tailwind('mt-14')
            : tailwind('mt-24'),
          {backgroundColor: this.props.color},
        ]}>
        <Text style={tailwind('text-black')}>
          {this.prepareTitle(this.props.title)}
        </Text>
      </TouchableOpacity>
    );
  }
}
