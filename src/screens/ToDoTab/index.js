import React from 'react';
import {
  View,
  Text,
  Modal,
  Alert,
  TouchableOpacity,
  TextInput,
  Platform,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {connect} from 'react-redux';
import {toDoActions} from 'src/actions/toDoActions';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ColorPicker from 'src/components/Notes/ColorPicker';
import Button from 'src/components/Utils/Button';
import removingItem from 'src/functions/utils/removingItem';

class TDScreen extends React.PureComponent {
  state = {
    modalVisible: false,
    expandedDone: false,
    expandedToDo: true,
    toDo: '',
    toDoColor: '#2793e8',
    colors: ['#cc3300', '#ff9966', '#ffcc00', '#99cc33', '#339900'],
  };

  componentDidMount() {
    this.props.navigation.setOptions({
      headerShown: true,
      headerTransparent: true,
      headerShadowVisible: false,
      title: 'KamilTasks',
      headerTitleAlign: 'center',
    });
  }

  changeText = string => {
    this.setState({toDo: string});
  };

  changeColor = color => {
    this.setState({toDoColor: color});
  };

  renderModal = () => {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          Alert.alert('Dodano zadanie!');
          setModalVisible(!this.state.modalVisible);
        }}>
        <View
          style={tailwind(
            'flex-1 justify-center items-center bg-black bg-opacity-50 px-5',
          )}>
          <View
            style={[
              tailwind('w-full border-black border items-center'),
              {backgroundColor: this.state.toDoColor},
            ]}>
            <Button
              type="close"
              size={30}
              color="black"
              onPress={() =>
                this.setState({modalVisible: !this.state.modalVisible})
              }
              style={tailwind('absolute top-0 right-0 m-1')}
            />
            <View
              style={tailwind(
                'flex justify-center text-black pt-5 items-center',
              )}>
              <Text>Wybierz priorytet</Text>
              <ColorPicker
                colors={this.state.colors}
                changeColor={this.changeColor}
              />
            </View>
            <Text style={tailwind('text-black my-4')}>
              Co masz dzisiaj do zrobienia ?
            </Text>
            <TextInput
              editable
              multiline
              placeholder={'Wprowadź tekst'}
              onChangeText={this.changeText}
              style={[
                tailwind(
                  'w-3/4 border-black border text-center rounded bg-gray-300',
                ),
                Platform.OS === 'ios' && tailwind('p-3'),
              ]}
            />
            <TouchableOpacity
              onPress={() => {
                this.props.addItem(this.state.toDo, this.state.toDoColor);
                console.debug(this.props.toDoList);
                this.setState({modalVisible: !this.state.modalVisible});
              }}
              style={tailwind(
                'border bg-gray-300 border-black py-3 my-4 px-10',
              )}>
              <Text style={tailwind('text-black')}>Dodaj</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  upperFirstletter = string => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  renderElement = item => {
    return (
      <TouchableOpacity
        key={item.id}
        onLongPress={() =>
          removingItem('KamilTasks', 'task', this.props.removeItem, item.id)
        }
        style={tailwind(
          'flex flex-row items-center justify-between w-full border border-black p-4 my-2 rounded bg-gray-200',
        )}>
        <View style={tailwind('flex flex-row items-center')}>
          <TouchableOpacity onPress={() => this.props.changeDone(item.id)}>
            <View
              style={tailwind(
                'flex items-center justify-center w-6 h-6 mr-2 bg-white rounded-md',
              )}>
              {item.isDone && <Icon name="check" size={25} color="black" />}
            </View>
          </TouchableOpacity>
          <View style={tailwind('flex flex-shrink w-5/6')}>
            <Text style={tailwind('text-justify')}>
              {this.upperFirstletter(item.toDo)}
            </Text>
          </View>
        </View>
        <View
          style={[
            tailwind('w-6 h-6 border-black border rounded-full'),
            {backgroundColor: item.color},
          ]}
        />
      </TouchableOpacity>
    );
  };

  renderList = (list, isDoneList) => {
    if (isDoneList) {
      return list.map(e => {
        if (e.isDone) {
          return this.renderElement(e);
        }
      });
    } else {
      return list.map(e => {
        if (!e.isDone) {
          return this.renderElement(e);
        }
      });
    }
  };

  renderExpand = expandType => {
    const iconDown = (
      <Icon
        name="chevron-down"
        size={40}
        color="black"
        style={tailwind('-ml-2')}
      />
    );
    const iconRight = (
      <Icon
        name="chevron-right"
        size={40}
        color="black"
        style={tailwind('-ml-2')}
      />
    );

    if (expandType === 'Done') {
      return (
        <TouchableOpacity
          onPress={() =>
            this.setState({expandedDone: !this.state.expandedDone})
          }>
          {this.state.expandedDone ? iconDown : iconRight}
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() =>
            this.setState({expandedToDo: !this.state.expandedToDo})
          }>
          {this.state.expandedToDo ? iconDown : iconRight}
        </TouchableOpacity>
      );
    }
  };

  render() {
    const commonStyles = tailwind('');

    return (
      <View
        style={[tailwind('flex-1 w-full items-center bg-white pt-20 px-4')]}>
        {this.renderModal()}
        <View style={tailwind('flex flex-col items-center w-full')}>
          <View
            style={tailwind('flex flex-row items-center justify-start w-full')}>
            {this.renderExpand('Done')}
            <Text style={tailwind('text-lg')}>Zadania zrobione</Text>
          </View>
          {this.state.expandedDone &&
            this.renderList(this.props.toDoList, true)}
        </View>
        <View
          style={tailwind('flex flex-row items-center justify-start w-full')}>
          {this.renderExpand('ToDo')}
          <Text style={tailwind('text-lg')}>Do zrobienia</Text>
        </View>
        {this.state.expandedToDo && this.renderList(this.props.toDoList, false)}
        <Button
          onPress={() =>
            this.setState({modalVisible: !this.state.modalVisible})
          }
          style={tailwind(
            'absolute bottom-0 right-0 m-4 p-0 border-black border rounded-full bg-white',
          )}
          type="plus"
          size={50}
          color="black"
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  toDoList: state.toDoList,
});

const mapDispatchToProps = dispatch => ({
  addItem: (toDo, color) => dispatch(toDoActions.addItem(toDo, color)),
  editItem: (id, toDo, color) =>
    dispatch(toDoActions.editItem(id, toDo, color)),
  removeItem: id => dispatch(toDoActions.removeItem(id)),
  changeDone: id => dispatch(toDoActions.changeDone(id)),
});

const ToDoScreen = connect(mapStateToProps, mapDispatchToProps)(TDScreen);

export default ToDoScreen;
