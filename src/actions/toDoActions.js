import {TODO_TYPES} from './types';

export const toDoActions = {
  addItem: (toDo, color) => {
    return {
      type: TODO_TYPES.ADD,
      payload: {toDo, color},
    };
  },

  editItem: (id, toDo, color) => {
    return {
      type: TODO_TYPES.EDIT,
      payload: {id, toDo, color},
    };
  },

  changeDone: id => {
    return {
      type: TODO_TYPES.ISDONE,
      payload: {id},
    };
  },

  removeItem: id => {
    return {
      type: TODO_TYPES.REMOVE,
      payload: id,
    };
  },
};
