import {NOTE_TYPES} from './types';

export const notesActions = {
  addNote: (note, title, color) => {
    return {
      type: NOTE_TYPES.ADD,
      payload: {note, title, color},
    };
  },

  editNote: (id, note, title, color) => {
    return {
      type: NOTE_TYPES.EDIT,
      payload: {id, note, title, color},
    };
  },

  removeNote: id => {
    return {
      type: NOTE_TYPES.REMOVE,
      payload: id,
    };
  },
};
