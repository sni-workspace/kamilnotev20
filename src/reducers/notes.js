import {NOTE_TYPES} from 'src/actions/types';
import uuid from 'react-native-uuid';

const initialState = [];

export const noteReducer = (state = initialState, action) => {
  const {type, payload} = action;
  let nextState;

  switch (type) {
    case NOTE_TYPES.ADD:
      {
        const arr = Object.values(state) || [];
        const len = state.length;

        arr.unshift({
          id: uuid.v4(),
          text: payload.note,
          title: payload.title,
          color: payload.color,
        });
        nextState = arr;
      }
      break;

    case NOTE_TYPES.EDIT:
      {
        const arr = Object.values(state) || [];
        arr.splice(
          arr.findIndex(e => e.id === payload.id),
          1,
        );

        arr.unshift({
          id: payload.id,
          text: payload.note,
          title: payload.title,
          color: payload.color,
        });

        nextState = arr;
      }
      break;

    case NOTE_TYPES.REMOVE:
      {
        const arr = Object.values(state) || [];
        nextState = arr.filter(e => e.id !== payload);
      }
      break;

    default:
      nextState = state;
      break;
  }

  return nextState ? nextState : state;
};
