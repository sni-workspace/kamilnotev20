import {TODO_TYPES} from 'src/actions/types';
import uuid from 'react-native-uuid';

const initialState = [];

export const toDoReducer = (state = initialState, action) => {
  const {type, payload} = action;
  let nextState;

  switch (type) {
    case TODO_TYPES.ADD:
      {
        const arr = Object.values(state) || [];

        console.log('reducer', payload);

        arr.unshift({
          id: uuid.v4(),
          toDo: payload.toDo,
          color: payload.color,
          isDone: false,
        });
        nextState = arr;
      }
      break;

    case TODO_TYPES.EDIT:
      {
        const arr = Object.values(state) || [];

        arr.splice(
          arr.findIndex(e => e.id === payload.id),
          1,
        );

        arr.unshift({
          id: payload.id,
          toDo: payload.toDo,
          color: payload.color,
          isDone: false,
        });

        nextState = arr;
      }
      break;

    case TODO_TYPES.ISDONE:
      {
        const arr = Object.values(state) || [];
        let selectedItem = {};

        Object.values(arr).map(e => {
          e.id === payload.id ? (selectedItem = e) : null;
        });

        arr.splice(
          arr.findIndex(e => e.id === payload.id),
          1,
        );
        arr.unshift({
          id: selectedItem.id,
          toDo: selectedItem.toDo,
          color: selectedItem.color,
          isDone: !selectedItem.isDone,
        });

        nextState = arr;
      }
      break;

    case TODO_TYPES.REMOVE:
      {
        const arr = Object.values(state) || [];
        nextState = arr.filter(e => e.id !== payload);
      }
      break;

    default:
      nextState = state;
      break;
  }

  return nextState ? nextState : state;
};
