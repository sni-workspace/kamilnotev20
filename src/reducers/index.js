import {combineReducers} from 'redux';

import {noteReducer} from './notes';
import {toDoReducer} from './toDo';

const rootReducer = combineReducers({
  notes: noteReducer,
  toDoList: toDoReducer,
});

export default rootReducer;
