import React from 'react';
import {View} from 'react-native';
import tailwind from 'tailwind-rn';

import ColorSelect from './ColorSelect';

export default class ColorPicker extends React.PureComponent {
  render() {
    return (
      <View style={tailwind('flex flex-row p-2')}>
        {this.props.colors.map(e => {
          return (
            <ColorSelect
              key={e}
              color={e}
              onPress={() => this.props.changeColor(e)}
            />
          );
        })}
      </View>
    );
  }
}
