import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import tailwind from 'tailwind-rn';

export default class ColorSelect extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View
          style={[
            tailwind('border-black border rounded-full p-4 m-1'),
            {backgroundColor: this.props.color},
          ]}
        />
      </TouchableOpacity>
    );
  }
}
