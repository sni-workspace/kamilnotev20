import React from 'react';
import {View, TextInput, Platform} from 'react-native';
import tailwind from 'tailwind-rn';

import ColorPicker from './ColorPicker';

export default class Input extends React.PureComponent {
  state = {
    colors: [
      '#F5D6C4',
      '#BCDEE3',
      '#D3EAFF',
      '#D291BC',
      '#CFCAE6',
      '#F0DAE3',
      '#FCF4E3',
      '#DEEDD5',
    ],
  };

  render() {
    return (
      <View
        style={[
          tailwind('flex-1 w-full items-center justify-start px-2'),
          // {backgroundColor: this.state.bgColor},
        ]}>
        <ColorPicker
          changeColor={this.props.changeColor}
          colors={this.state.colors}
        />
        <TextInput
          editable
          value={this.props.title}
          onChangeText={this.props.changeTitle}
          style={[
            tailwind(
              'w-full border-black border text-center m-2 rounded-md bg-gray-200',
            ),
            Platform.OS === 'ios' && tailwind('p-4'),
          ]}
        />
        <TextInput
          editable
          multiline
          value={this.props.text}
          onChangeText={this.props.changeText}
          style={[
            tailwind(
              'flex-1 w-full border-black border m-2 rounded-md p-4 bg-gray-200',
            ),
            {textAlignVertical: 'top'},
          ]}
        />
      </View>
    );
  }
}
