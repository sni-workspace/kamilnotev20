import React from 'react';
import {TouchableOpacity} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Button extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity style={this.props.style} onPress={this.props.onPress}>
        <Icon
          name={this.props.type}
          size={this.props.size}
          color={this.props.color}
        />
      </TouchableOpacity>
    );
  }
}
